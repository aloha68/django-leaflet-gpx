from django.apps import AppConfig


class LeafletGpxConfig(AppConfig):
    name = 'django_leaflet_gpx'
    verbose_name = "Leaflet GPX"
