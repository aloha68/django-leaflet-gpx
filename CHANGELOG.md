# Changelog
All notable changes to this project will be documented in this file.

## 0.1.3 - 2019-10-06

### Changed
- Javascript variable name is now unique and allow to include maps multiple times in the same HTML page

## 0.1.2 - 2019-10-05

### Changed
- Default max interval point is set to a minute

## 0.1.1 - 2019-06-27

### Added
- Default icons for markers, GPX start and GPX end
- Overwritable LEAFLET_GPX_ICONS settings to override default icons
- Template tags 'simple_map' and 'gpx_map' can be directly used in python

### Changed
- Update Leaflet-JS to 1.5.1

### Fixed
- It is possible to define only some of default icons

## 0.1.0 - 2019-06-21
First release
